/*
 * DNN Classifier.
 * Please note that this classifier actually outputs 7 discriminator values simultaneously.They can
 * be interpreted as a classification probability as they sum up to 1. Classes (order is important):
 * ttH, ttbb, ttb, tt2b, ttcc, ttlf, other
 */

#ifndef TTH_DNNCLASSIFIER_H
#define TTH_DNNCLASSIFIER_H

#include <algorithm>
#include <iterator>
#include <map>
#include <vector>

#include "Python.h"

#include "TLorentzVector.h"
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"
#include "TVectorD.h"

#include "TTH/CommonClassifier/interface/CommonBDTvars.h"

typedef std::vector<double> doubles;

class DNNOutput
{
public:
    std::vector<double> values;

    DNNOutput(double ttH, double ttbb, double ttb, double tt2b, double ttcc, double ttlf,
        double other)
    {
        values.push_back(ttH);
        values.push_back(ttbb);
        values.push_back(ttb);
        values.push_back(tt2b);
        values.push_back(ttcc);
        values.push_back(ttlf);
        values.push_back(other);
    }

    DNNOutput()
        : DNNOutput(0., 0., 0., 0., 0., 0., 0.)
    {
        reset();
    }

    ~DNNOutput()
    {
    }

    void reset();

    inline double ttH() const
    {
        return values[0];
    }

    inline double ttbb() const
    {
        return values[1];
    }

    inline double ttb() const
    {
        return values[2];
    }

    inline double tt2b() const
    {
        return values[3];
    }

    inline double ttcc() const
    {
        return values[4];
    }

    inline double ttlf() const
    {
        return values[5];
    }

    inline double other() const
    {
        return values[6];
    }

    inline size_t mostProbableClass() const
    {
        return distance(values.begin(), max_element(values.begin(), values.end()));
    }
};

class DNNVariables
{
public:
    double getHt(const std::vector<const TLorentzVector*>& lvecs) const;

    void getMinMaxDR(const std::vector<const TLorentzVector*>& lvecs, double& minDR,
        double& maxDR) const;

    void getMinMaxDR(const std::vector<const TLorentzVector*>& lvecs, const TLorentzVector* lvec,
        double& minDR, double& maxDR) const;

    double getCentrality(const std::vector<const TLorentzVector*>& lvecs) const;

    void getSphericalEigenValues(const std::vector<const TLorentzVector*>& lvecs, double& ev1,
        double& ev2, double& ev3) const;

    double cosTheta(const TLorentzVector* particle1, const TLorentzVector* particle2) const;

    double legendreN(int n, const TLorentzVector* p1, const TLorentzVector* p2) const;

    double foxWolframN(int n, const std::vector<const TLorentzVector*>& particles) const;

    void getCSVStats(const doubles& jetCSVs, double& maxCSV, double& minCSV, double& meanCSV) const;

    double getMassLepClosestJet(const TLorentzVector* lep, const std::vector<const TLorentzVector*>& lvecs) const;

    void getJetVars(const std::vector<const TLorentzVector*>& lvecs, const doubles& jetCSVs,
        double& avgDEta, double& avgDR, double& avgCSV, double& dev2CSV, double& closestPt,
        double& closestMass, double& closestMass125) const;

    void getMassAverages(const std::vector<const TLorentzVector*>& lvecs, double& avgMass,
        double& avgMass2) const;

    double getSecondHighestValue(const doubles& vals) const;
};

class DNNClassifierBase
{
public:
    double csvCutMeadium;
    double csvCutTight;

    DNNClassifierBase(std::string version);

    virtual ~DNNClassifierBase();

    static void pyInitialize();
    static void pyFinalize();
    static void pyExcept(PyObject* pyObj, const std::string& msg);

protected:
    string version_;
    string inputName_;
    string outputName_;
    string dropoutName_;
    PyObject* pyContext_;
    PyObject* pyEval_;
    DNNVariables dnnVars_;
};

class DNNClassifier_SL : public DNNClassifierBase
{
public:
    DNNClassifier_SL(std::string version = "v6a");

    ~DNNClassifier_SL();

    void evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
        const TLorentzVector& lepton, const TLorentzVector& met, const doubles& additionalFeatures,
        DNNOutput& dnnOutput);

    DNNOutput evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
        const TLorentzVector& lepton, const TLorentzVector& met, const doubles& additionalFeatures);

    void fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
        const doubles& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met,
        const doubles& additionalFeatures);

private:
    size_t nFeatures4_;
    size_t nFeatures5_;
    size_t nFeatures6_;
    PyObject* pyEvalArgs4_;
    PyObject* pyEvalArgs5_;
    PyObject* pyEvalArgs6_;
    CommonBDTvars bdtVars_;
};

class DNNClassifier_DL : public DNNClassifierBase
{
public:
    DNNClassifier_DL(std::string version = "v3a");

    ~DNNClassifier_DL();

    void evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
        const std::vector<TLorentzVector>& leptons, const TLorentzVector& met,
        const doubles& additionalFeatures, DNNOutput& dnnOutput);

    DNNOutput evaluate(const std::vector<TLorentzVector>& jets, const doubles& jetCSVs,
        const std::vector<TLorentzVector>& leptons, const TLorentzVector& met,
        const doubles& additionalFeatures);

    void fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
        const doubles& jetCSVs, const std::vector<TLorentzVector>& leptons,
        const TLorentzVector& met, const doubles& additionalFeatures);

private:
    size_t nFeatures3_;
    size_t nFeatures4_;
    PyObject* pyEvalArgs3_;
    PyObject* pyEvalArgs4_;
};

// python evaluation script
static string evalScript = "\
import sys, numpy as np\n\
td, inputs, outputs, dropouts = None, [], [], []\n\
def setup(python_path, model_files, input_name, output_name, dropout_name):\n\
    global td, inputs, outputs, dropouts\n\
    sys.path.insert(0, python_path)\n\
    import tfdeploy as td\n\
    for model_file in model_files:\n\
        model = td.Model(model_file)\n\
        inputs.append(model.get(input_name))\n\
        outputs.append(model.get(output_name))\n\
        dropouts.append(model.get(dropout_name))\n\
def eval(m, *values):\n\
    return list(outputs[m].eval({inputs[m]: [np.array(values).astype(np.float32)], dropouts[m]: 1.})[0])\n\
";

#endif // TTH_DNNCLASSIFIER_H
