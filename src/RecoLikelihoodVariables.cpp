#include "../interface/RecoLikelihoodVariables.h"

#include <iostream>

using namespace std;

RecoLikelihoodVariables::RecoLikelihoodVariables(){

  //Deep CSV working points for 94x as of 23.04.2018
  CSVLwp = 0.1522;
  CSVMwp = 0.4941;
  CSVTwp = 0.8001;


    tags_tth.push_back("TTHLikelihood");
    tags_ttbb.push_back("TTBBLikelihood");
    tags_tt.push_back("TTLikelihood");
    tags_tt.push_back("TTLikelihood_comb");
    tags_tth.push_back("TTHLikelihoodTimesME");
    tags_ttbb.push_back("TTBBLikelihoodTimesME");
    alltags.insert(alltags.end(), tags_tth.begin(), tags_tth.end());
    alltags.insert(alltags.end(), tags_tt.begin(), tags_tt.end());
    alltags.insert(alltags.end(), tags_ttbb.begin(), tags_ttbb.end());
    ratiotags_tth.push_back("TTHLikelihood");
    ratiotags_tth.push_back("TTHLikelihoodTimesME");
    ratiotags_ttbb.push_back("TTBBLikelihood");
    ratiotags_ttbb.push_back("TTBBLikelihoodTimesME");
    ratiotags_name.push_back("Likelihood");
    ratiotags_name.push_back("LikelihoodTimesME");

}


void RecoLikelihoodVariables::CalculateRecoLikelihoodVariables(const vector<TLorentzVector>& selectedLeptonP4,
                                                               const vector<TLorentzVector>& selectedJetP4,
                                                               const vector<double>&         selectedJetCSV,
                                                               const TLorentzVector&         metP4)
{

    if(selectedJetP4.size()<6) return;
    // get vectors
    vector<TLorentzVector> jetvecs = selectedJetP4; 
    vector<float> jetcsvs(selectedJetCSV.begin(), selectedJetCSV.end());
    int ntags=0;
    for(auto j=jetcsvs.begin(); j!=jetcsvs.end(); j++){
        if(*j > CSVMwp) ntags++;
    }
    if(ntags<2) return;
    if(selectedLeptonP4.size()<1) return;
    TLorentzVector lepvec = selectedLeptonP4[0];
    TVector2 metvec(metP4.Px(), metP4.Py());



    // generate interpretations
    Interpretation** ints = generator.GenerateTTHInterpretations(jetvecs,jetcsvs,lepvec,metvec);
    uint nints = generator.GetNints();
    
    // calculate best tags and interpretations
    map<string,Interpretation*> best_int;
    for(auto tagname=alltags.begin();tagname!=alltags.end();tagname++){
	best_int[*tagname]=0;
    }
    map<string,float> best_tag;
    for(uint i=0;i<nints;i++){
	for(auto tagname=alltags.begin();tagname!=alltags.end();tagname++){
	    float tag=quality.GetTag(*tagname,*ints[i]);
	    if(tag>best_tag[*tagname]){
		best_int[*tagname]=ints[i];
		best_tag[*tagname]=tag;
	    }
	}
    }


    // calculate variables for best tt interpretations
    {
        string tagname = "TTLikelihood";
	Interpretation* bi=best_int[tagname];
	if(bi!=0){
            m_Reco_Dr_BB_best_TTLikelihood   = bi->B1().DeltaR(bi->B2());
            m_Reco_Higgs_M_best_TTLikelihood = best_int[tagname]->Higgs_M();

            double tthlike= quality.TTHLikelihood(*bi);
            double tthme = quality.TTHBB_ME(*bi);
            double tthlikeme = tthlike*tthme;
            double ttbblike= quality.TTBBLikelihood(*bi);
            double ttbbme = quality.TTBB_ON_ME(*bi);
            double ttbblikeme = ttbblike*ttbbme;
            double ttbbme_off = quality.TTBB_OFF_ME(*bi);
            double ttbblikeme_off = ttbblike*ttbbme_off;
            double like_ratio = tthlike/(tthlike+ttbblike);
            double me_ratio = tthme/(tthme+ttbbme);
            double me_ratio_off = tthme/(tthme+ttbbme_off);
            double likeme_ratio = tthlikeme/(tthlikeme+ttbblikeme);
            double likeme_ratio_off = tthlikeme/(tthlikeme+ttbblikeme_off);

            m_Reco_TTHLikelihood_best_TTLikelihood              = tthlike;
            m_Reco_TTHBBME_best_TTLikelihood                    = tthme;
            m_Reco_TTBBLikelihood_best_TTLikelihood             = ttbblike;
            m_Reco_TTBBME_best_TTLikelihood                     = ttbbme;
            m_Reco_TTBBME_off_best_TTLikelihood                 = ttbbme_off;
            m_Reco_TTHLikelihoodTimesME_best_TTLikelihood       = tthlikeme;
            m_Reco_TTBBLikelihoodTimesME_best_TTLikelihood      = ttbblikeme;
            m_Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood  = ttbblikeme_off;
            m_Reco_LikelihoodRatio_best_TTLikelihood            = like_ratio;
            m_Reco_MERatio_best_TTLikelihood                    = me_ratio;
            m_Reco_MERatio_off_best_TTLikelihood                = me_ratio_off;
            m_Reco_LikelihoodTimesMERatio_best_TTLikelihood     = likeme_ratio;
            m_Reco_LikelihoodTimesMERatio_off_best_TTLikelihood = likeme_ratio_off;
        }
    }
    {
        string tagname = "TTLikelihood_comb";
        Interpretation* bi=best_int[tagname];
        if(bi!=0){
            m_Reco_Dr_BB_best_TTLikelihood_comb   = bi->B1().DeltaR(bi->B2());
            m_Reco_Higgs_M_best_TTLikelihood_comb = best_int[tagname]->Higgs_M();
            
            double tthlike= quality.TTHLikelihood(*bi);
            double tthme = quality.TTHBB_ME(*bi);
            double tthlikeme = tthlike*tthme;
            double ttbblike= quality.TTBBLikelihood(*bi);
            double ttbbme = quality.TTBB_ON_ME(*bi);
            double ttbblikeme = ttbblike*ttbbme;
            double ttbbme_off = quality.TTBB_OFF_ME(*bi);
            double ttbblikeme_off = ttbblike*ttbbme_off;
            double like_ratio = tthlike/(tthlike+ttbblike);
            double me_ratio = tthme/(tthme+ttbbme);
            double me_ratio_off = tthme/(tthme+ttbbme_off);
            double likeme_ratio = tthlikeme/(tthlikeme+ttbblikeme);
            double likeme_ratio_off = tthlikeme/(tthlikeme+ttbblikeme_off);
            
            m_Reco_TTHLikelihood_best_TTLikelihood_comb              = tthlike;
            m_Reco_TTHBBME_best_TTLikelihood_comb                    = tthme;
            m_Reco_TTBBLikelihood_best_TTLikelihood_comb             = ttbblike;
            m_Reco_TTBBME_best_TTLikelihood_comb                     = ttbbme;
            m_Reco_TTBBME_off_best_TTLikelihood_comb                 = ttbbme_off;
            m_Reco_TTHLikelihoodTimesME_best_TTLikelihood_comb       = tthlikeme;
            m_Reco_TTBBLikelihoodTimesME_best_TTLikelihood_comb      = ttbblikeme;
            m_Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood_comb  = ttbblikeme_off;
            m_Reco_LikelihoodRatio_best_TTLikelihood_comb            = like_ratio;
            m_Reco_MERatio_best_TTLikelihood_comb                    = me_ratio;
            m_Reco_MERatio_off_best_TTLikelihood_comb                = me_ratio_off;
            m_Reco_LikelihoodTimesMERatio_best_TTLikelihood_comb     = likeme_ratio;
            m_Reco_LikelihoodTimesMERatio_off_best_TTLikelihood_comb = likeme_ratio_off;
        }
    }


    {
        string tagname = "TTHLikelihood";
	Interpretation* bi=best_int[tagname];
	if(bi!=0){
            double tthlike= quality.TTHLikelihood(*bi);
            double tthme = quality.TTHBB_ME(*bi);
            double tthlikeme = tthlike*tthme;

            m_Reco_TTHLikelihood_best_TTHLikelihood        = tthlike;
            m_Reco_TTHBBME_best_TTHLikelihood              = tthme;
            m_Reco_TTHLikelihoodTimesME_best_TTHLikelihood = tthlikeme;
        }
    }
    {
        string tagname = "TTHLikelihoodTimesME";
        Interpretation* bi=best_int[tagname];
        if(bi!=0){
            double tthlike= quality.TTHLikelihood(*bi);
            double tthme = quality.TTHBB_ME(*bi);
            double tthlikeme = tthlike*tthme;
            
            m_Reco_TTHLikelihood_best_TTHLikelihoodTimesME        = tthlike;
            m_Reco_TTHBBME_best_TTHLikelihoodTimesME              = tthme;
            m_Reco_TTHLikelihoodTimesME_best_TTHLikelihoodTimesME = tthlikeme;
        }
    }


    {
        string tagname = "TTBBLikelihood";
	Interpretation* bi=best_int[tagname];
	if(bi!=0){
            double ttbblike= quality.TTBBLikelihood(*bi);
            double ttbbme = quality.TTBB_ON_ME(*bi);
            double ttbblikeme = ttbblike*ttbbme;
            double ttbbme_off = quality.TTBB_OFF_ME(*bi);
            double ttbblikeme_off = ttbblike*ttbbme_off;

            m_Reco_TTBBLikelihood_best_TTBBLikelihood            = ttbblike;
            m_Reco_TTBBME_best_TTBBLikelihood                    = ttbbme;
            m_Reco_TTBBLikelihoodTimesME_best_TTBBLikelihood     = ttbblikeme;
            m_Reco_TTBBME_off_best_TTBBLikelihood                = ttbbme_off;
            m_Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihood = ttbblikeme_off;

            float dEta_fn=sqrt(abs((bi->Higgs().Eta() - bi->TopLep().Eta())*(bi->Higgs().Eta() - bi->TopHad().Eta())));

            m_Reco_Deta_Fn_best_TTBBLikelihood        = dEta_fn;
            m_Reco_Deta_TopLep_BB_best_TTBBLikelihood = abs(bi->Higgs().Eta() - bi->TopLep().Eta());
            m_Reco_Deta_TopHad_BB_best_TTBBLikelihood = abs(bi->Higgs().Eta() - bi->TopHad().Eta());
        }
    }
    {
        string tagname = "TTBBLikelihoodTimesME";
        Interpretation* bi=best_int[tagname];
        if(bi!=0){
            double ttbblike= quality.TTBBLikelihood(*bi);
            double ttbbme = quality.TTBB_ON_ME(*bi);
            double ttbblikeme = ttbblike*ttbbme;
            double ttbbme_off = quality.TTBB_OFF_ME(*bi);
            double ttbblikeme_off = ttbblike*ttbbme_off;

            m_Reco_TTBBLikelihood_best_TTBBLikelihoodTimesME            = ttbblike;
            m_Reco_TTBBME_best_TTBBLikelihoodTimesME                    = ttbbme;
            m_Reco_TTBBLikelihoodTimesME_best_TTBBLikelihoodTimesME     = ttbblikeme;
            m_Reco_TTBBME_off_best_TTBBLikelihoodTimesME                = ttbbme_off;
            m_Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihoodTimesME = ttbblikeme_off;
            
            float dEta_fn=sqrt(abs((bi->Higgs().Eta() - bi->TopLep().Eta())*(bi->Higgs().Eta() - bi->TopHad().Eta())));
            
            m_Reco_Deta_Fn_best_TTBBLikelihoodTimesME        = dEta_fn;
            m_Reco_Deta_TopLep_BB_best_TTBBLikelihoodTimesME = abs(bi->Higgs().Eta() - bi->TopLep().Eta());
            m_Reco_Deta_TopHad_BB_best_TTBBLikelihoodTimesME = abs(bi->Higgs().Eta() - bi->TopHad().Eta());
        }
    }


    // fill best tth / best ttbb interpretation ratios
    assert(ratiotags_tth.size()==ratiotags_ttbb.size());
    assert(ratiotags_tth.size()==ratiotags_name.size());
    {
        uint i=0;
	Interpretation* bi_tth=best_int[ratiotags_tth[i]];
	Interpretation* bi_ttbb=best_int[ratiotags_ttbb[i]];
	if(bi_tth!=0){
            if(bi_ttbb!=0){
                double tthlike= quality.TTHLikelihood(*bi_tth);
                double tthme = quality.TTHBB_ME(*bi_tth);
                double tthlikeme = tthlike*tthme;
                double ttbblike= quality.TTBBLikelihood(*bi_ttbb);
                double ttbbme = quality.TTBB_ON_ME(*bi_ttbb);
                double ttbblikeme = ttbblike*ttbbme;
                double ttbbme_off = quality.TTBB_OFF_ME(*bi_ttbb);
                double ttbblikeme_off = ttbblike*ttbbme_off;
                double like_ratio = tthlike/(tthlike+ttbblike);
                double me_ratio = tthme/(tthme+ttbbme);
                double me_ratio_off = tthme/(tthme+ttbbme_off);
                double likeme_ratio = tthlikeme/(tthlikeme+ttbblikeme);
                double likeme_ratio_off = tthlikeme/(tthlikeme+ttbblikeme_off);


                m_Reco_LikelihoodRatio_best_Likelihood            = like_ratio;
                m_Reco_MERatio_best_Likelihood                    = me_ratio;
                m_Reco_MERatio_off_best_Likelihood                = me_ratio_off;
                m_Reco_LikelihoodTimesMERatio_best_Likelihood     = likeme_ratio;
                m_Reco_LikelihoodTimesMERatio_off_best_Likelihood = likeme_ratio_off;
            }
        }
    }
    {
        uint i=1;
        Interpretation* bi_tth=best_int[ratiotags_tth[i]];
        Interpretation* bi_ttbb=best_int[ratiotags_ttbb[i]];
        if(bi_tth!=0){
            if(bi_ttbb!=0){
                double tthlike= quality.TTHLikelihood(*bi_tth);
                double tthme = quality.TTHBB_ME(*bi_tth);
                double tthlikeme = tthlike*tthme;
                double ttbblike= quality.TTBBLikelihood(*bi_ttbb);
                double ttbbme = quality.TTBB_ON_ME(*bi_ttbb);
                double ttbblikeme = ttbblike*ttbbme;
                double ttbbme_off = quality.TTBB_OFF_ME(*bi_ttbb);
                double ttbblikeme_off = ttbblike*ttbbme_off;
                double like_ratio = tthlike/(tthlike+ttbblike);
                double me_ratio = tthme/(tthme+ttbbme);
                double me_ratio_off = tthme/(tthme+ttbbme_off);
                double likeme_ratio = tthlikeme/(tthlikeme+ttbblikeme);
                double likeme_ratio_off = tthlikeme/(tthlikeme+ttbblikeme_off);


                m_Reco_LikelihoodRatio_best_LikelihoodTimesME            = like_ratio;
                m_Reco_MERatio_best_LikelihoodTimesME                    = me_ratio;
                m_Reco_MERatio_off_best_LikelihoodTimesME                = me_ratio_off;
                m_Reco_LikelihoodTimesMERatio_best_LikelihoodTimesME     = likeme_ratio;
                m_Reco_LikelihoodTimesMERatio_off_best_LikelihoodTimesME = likeme_ratio_off;
            }
        }
    }


    // sum up likelihoods
    double sum_tth_likelihood=0;
    double sum_ttbb_likelihood=0; 
    double sum_tth_me_likelihood=0;
    double sum_ttbb_me_likelihood=0;
    double sum_tth_me=0;
    double sum_ttbb_me=0;   
    for(uint i=0;i<nints;i++){
	// calculating MEs for many interpretations can take a lot of time
	double tthlike= quality.TTHLikelihood(*ints[i]);
	double tthme = quality.TTHBB_ME(*ints[i]);
	double tthlikeme = tthlike*tthme;
	double ttbblike= quality.TTBBLikelihood(*ints[i]);
	double ttbbme = quality.TTBB_ON_ME(*ints[i]);
	double ttbblikeme = ttbblike*ttbbme;
	sum_tth_likelihood+=tthlike;
	sum_tth_me+=tthme;
	sum_tth_me_likelihood+=tthlikeme;
	sum_ttbb_likelihood+=ttbblike;
	sum_ttbb_me+=ttbbme;
	sum_ttbb_me_likelihood+=ttbblikeme;
    }
    m_Reco_Sum_TTHLikelihood         = sum_tth_likelihood;
    m_Reco_Sum_TTBBLikelihood        = sum_ttbb_likelihood;
    m_Reco_Sum_TTHLikelihoodTimesME  = sum_tth_me_likelihood;
    m_Reco_Sum_TTBBLikelihoodTimesME = sum_ttbb_me_likelihood;
    m_Reco_Sum_TTHBBME               = sum_tth_me;
    m_Reco_Sum_TTBBME                = sum_ttbb_me;

    m_Reco_Sum_LikelihoodRatio        = sum_tth_likelihood/(sum_tth_likelihood+sum_ttbb_likelihood);
    m_Reco_Sum_LikelihoodTimesMERatio = sum_tth_me_likelihood/(sum_tth_me_likelihood+sum_ttbb_me_likelihood);
    m_Reco_Sum_MERatio                = sum_tth_me/(sum_tth_me+sum_ttbb_me);



    return;

}




std::map<TString,double> RecoLikelihoodVariables::GetRecoLikelihoodVariables() {
    
    std::map<TString,double> recoLikelihoodVariables;
    
    recoLikelihoodVariables["Reco_Dr_BB_best_TTLikelihood"]                              = m_Reco_Dr_BB_best_TTLikelihood;
    recoLikelihoodVariables["Reco_Dr_BB_best_TTLikelihood_comb"]                         = m_Reco_Dr_BB_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_Higgs_M_best_TTLikelihood"]                            = m_Reco_Higgs_M_best_TTLikelihood;
    recoLikelihoodVariables["Reco_Higgs_M_best_TTLikelihood_comb"]                       = m_Reco_Higgs_M_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTHLikelihood_best_TTLikelihood"]                      = m_Reco_TTHLikelihood_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTHLikelihood_best_TTLikelihood_comb"]                 = m_Reco_TTHLikelihood_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTHBBME_best_TTLikelihood"]                            = m_Reco_TTHBBME_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTHBBME_best_TTLikelihood_comb"]                       = m_Reco_TTHBBME_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTHLikelihoodTimesME_best_TTLikelihood"]               = m_Reco_TTHLikelihoodTimesME_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTHLikelihoodTimesME_best_TTLikelihood_comb"]          = m_Reco_TTHLikelihoodTimesME_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTBBLikelihood_best_TTLikelihood"]                     = m_Reco_TTBBLikelihood_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTBBLikelihood_best_TTLikelihood_comb"]                = m_Reco_TTBBLikelihood_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTBBME_best_TTLikelihood"]                             = m_Reco_TTBBME_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTBBME_best_TTLikelihood_comb"]                        = m_Reco_TTBBME_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTBBME_off_best_TTLikelihood"]                         = m_Reco_TTBBME_off_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTBBME_off_best_TTLikelihood_comb"]                    = m_Reco_TTBBME_off_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_best_TTLikelihood"]              = m_Reco_TTBBLikelihoodTimesME_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_best_TTLikelihood_comb"]         = m_Reco_TTBBLikelihoodTimesME_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood"]          = m_Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood_comb"]     = m_Reco_TTBBLikelihoodTimesME_off_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_LikelihoodRatio_best_TTLikelihood"]                    = m_Reco_LikelihoodRatio_best_TTLikelihood;
    recoLikelihoodVariables["Reco_LikelihoodRatio_best_TTLikelihood_comb"]               = m_Reco_LikelihoodRatio_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_MERatio_best_TTLikelihood"]                            = m_Reco_MERatio_best_TTLikelihood;
    recoLikelihoodVariables["Reco_MERatio_best_TTLikelihood_comb"]                       = m_Reco_MERatio_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_MERatio_off_best_TTLikelihood"]                        = m_Reco_MERatio_off_best_TTLikelihood;
    recoLikelihoodVariables["Reco_MERatio_off_best_TTLikelihood_comb"]                   = m_Reco_MERatio_off_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_best_TTLikelihood"]             = m_Reco_LikelihoodTimesMERatio_best_TTLikelihood;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_best_TTLikelihood_comb"]        = m_Reco_LikelihoodTimesMERatio_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_off_best_TTLikelihood"]         = m_Reco_LikelihoodTimesMERatio_off_best_TTLikelihood;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_off_best_TTLikelihood_comb"]    = m_Reco_LikelihoodTimesMERatio_off_best_TTLikelihood_comb;
    recoLikelihoodVariables["Reco_TTHLikelihood_best_TTHLikelihood"]                     = m_Reco_TTHLikelihood_best_TTHLikelihood;
    recoLikelihoodVariables["Reco_TTHLikelihood_best_TTHLikelihoodTimesME"]              = m_Reco_TTHLikelihood_best_TTHLikelihoodTimesME;
    recoLikelihoodVariables["Reco_TTHBBME_best_TTHLikelihood"]                           = m_Reco_TTHBBME_best_TTHLikelihood;
    recoLikelihoodVariables["Reco_TTHBBME_best_TTHLikelihoodTimesME"]                    = m_Reco_TTHBBME_best_TTHLikelihoodTimesME;
    recoLikelihoodVariables["Reco_TTHLikelihoodTimesME_best_TTHLikelihood"]              = m_Reco_TTHLikelihoodTimesME_best_TTHLikelihood;
    recoLikelihoodVariables["Reco_TTHLikelihoodTimesME_best_TTHLikelihoodTimesME"]       = m_Reco_TTHLikelihoodTimesME_best_TTHLikelihoodTimesME;
    recoLikelihoodVariables["Reco_TTBBLikelihood_best_TTBBLikelihood"]                   = m_Reco_TTBBLikelihood_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_TTBBLikelihood_best_TTBBLikelihoodTimesME"]            = m_Reco_TTBBLikelihood_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_TTBBME_best_TTBBLikelihood"]                           = m_Reco_TTBBME_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_TTBBME_best_TTBBLikelihoodTimesME"]                    = m_Reco_TTBBME_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_best_TTBBLikelihood"]            = m_Reco_TTBBLikelihoodTimesME_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_best_TTBBLikelihoodTimesME"]     = m_Reco_TTBBLikelihoodTimesME_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_TTBBME_off_best_TTBBLikelihood"]                       = m_Reco_TTBBME_off_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_TTBBME_off_best_TTBBLikelihoodTimesME"]                = m_Reco_TTBBME_off_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihood"]        = m_Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihoodTimesME"] = m_Reco_TTBBLikelihoodTimesME_off_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_Deta_Fn_best_TTBBLikelihood"]                          = m_Reco_Deta_Fn_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_Deta_Fn_best_TTBBLikelihoodTimesME"]                   = m_Reco_Deta_Fn_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_Deta_TopLep_BB_best_TTBBLikelihood"]                   = m_Reco_Deta_TopLep_BB_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_Deta_TopLep_BB_best_TTBBLikelihoodTimesME"]            = m_Reco_Deta_TopLep_BB_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_Deta_TopHad_BB_best_TTBBLikelihood"]                   = m_Reco_Deta_TopHad_BB_best_TTBBLikelihood;
    recoLikelihoodVariables["Reco_Deta_TopHad_BB_best_TTBBLikelihoodTimesME"]            = m_Reco_Deta_TopHad_BB_best_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_LikelihoodRatio_best_Likelihood"]                      = m_Reco_LikelihoodRatio_best_Likelihood;
    recoLikelihoodVariables["Reco_LikelihoodRatio_best_LikelihoodTimesME"]               = m_Reco_LikelihoodRatio_best_LikelihoodTimesME;
    recoLikelihoodVariables["Reco_MERatio_best_Likelihood"]                              = m_Reco_MERatio_best_Likelihood;
    recoLikelihoodVariables["Reco_MERatio_best_LikelihoodTimesME"]                       = m_Reco_MERatio_best_LikelihoodTimesME;
    recoLikelihoodVariables["Reco_MERatio_off_best_Likelihood"]                          = m_Reco_MERatio_off_best_Likelihood;
    recoLikelihoodVariables["Reco_MERatio_off_best_LikelihoodTimesME"]                   = m_Reco_MERatio_off_best_LikelihoodTimesME;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_best_Likelihood"]               = m_Reco_LikelihoodTimesMERatio_best_Likelihood;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_best_LikelihoodTimesME"]        = m_Reco_LikelihoodTimesMERatio_best_LikelihoodTimesME;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_off_best_Likelihood"]           = m_Reco_LikelihoodTimesMERatio_off_best_Likelihood;
    recoLikelihoodVariables["Reco_LikelihoodTimesMERatio_off_best_LikelihoodTimesME"]    = m_Reco_LikelihoodTimesMERatio_off_best_LikelihoodTimesME;
    recoLikelihoodVariables["Reco_Sum_TTHLikelihood"]                                    = m_Reco_Sum_TTHLikelihood;
    recoLikelihoodVariables["Reco_Sum_TTBBLikelihood"]                                   = m_Reco_Sum_TTBBLikelihood;
    recoLikelihoodVariables["Reco_Sum_TTHLikelihoodTimesME"]                             = m_Reco_Sum_TTHLikelihoodTimesME;
    recoLikelihoodVariables["Reco_Sum_TTBBLikelihoodTimesME"]                            = m_Reco_Sum_TTBBLikelihoodTimesME;
    recoLikelihoodVariables["Reco_Sum_TTHBBME"]                                          = m_Reco_Sum_TTHBBME;
    recoLikelihoodVariables["Reco_Sum_TTBBME"]                                           = m_Reco_Sum_TTBBME;
    recoLikelihoodVariables["Reco_Sum_LikelihoodRatio"]                                  = m_Reco_Sum_LikelihoodRatio;
    recoLikelihoodVariables["Reco_Sum_LikelihoodTimesMERatio"]                           = m_Reco_Sum_LikelihoodTimesMERatio;
    recoLikelihoodVariables["Reco_Sum_MERatio"]                                          = m_Reco_Sum_MERatio;


    return recoLikelihoodVariables;

}

