action() {
	if [ -e "${CMSSW_BASE}" ]; then
		local origin="$( pwd )"

		# source this script to install the MEM package
		cd $CMSSW_BASE

		# make the MEM install dir
		mkdir -p src/TTH
		cd src/TTH

		# get the MEM code
		if [ -d MEIntegratorStandalone ]; then
		    echo "MEIntegratorStandalone dir already exists, repository isn't cloned again."
		else
		    git clone https://gitlab.cern.ch/Zurich_ttH/MEIntegratorStandalone.git MEIntegratorStandalone --branch v0.4
		fi    
		
		# copy the OpenLoops ME libraries
		mkdir -p ../../lib/$SCRAM_ARCH/
		cp -R MEIntegratorStandalone/libs/* ../../lib/$SCRAM_ARCH/
		eval `scramv1 runtime -sh`
		ls
		
		scram setup MEIntegratorStandalone/deps/gsl.xml

		cd $origin
	fi
}
action "$@"
